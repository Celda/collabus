package com.example.collabus;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;




import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.collabus.Helper.Config;
import com.example.collabus.Model.ServerResponse;

import org.w3c.dom.Text;


public class DisplayDataActivity extends AppCompatActivity {

    private Button button;
    private TextView textViewName, textViewSurname, textViewType, textViewemail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_data);



        button = (Button) findViewById(R.id.closeApp);
        textViewName = (TextView) findViewById(R.id.name);
        textViewSurname = (TextView) findViewById(R.id.surname);
        textViewType = (TextView) findViewById(R.id.type);
        textViewemail = (TextView) findViewById(R.id.email);

        Intent i = getIntent();
        ServerResponse dene = (ServerResponse)i.getSerializableExtra("sampleObject");

        if (!dene.getNom().isEmpty()) {
            textViewName.setText("Nom de famille: " + dene.getNom());
        }
        if (!dene.getPrenoms().isEmpty()) {
            textViewSurname.setText("Prénoms: " + dene.getPrenoms());
        }

        if(!dene.getEmail().isEmpty()) {
            textViewemail.setText("Email: " + dene.getEmail());
        }

        if (!dene.getType().toString().isEmpty()) {
            textViewType.setText("Type: " + dene.getType().toString());
        }



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //finish();
                //System.exit(0);
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                System.exit(1);
            }
        });
    }
}