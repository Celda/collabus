package com.example.collabus.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Signin {
    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("passe")
    @Expose
    private String password;

    public Signin() {
    }

    public Signin(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

