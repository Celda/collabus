package com.example.collabus.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Example implements Serializable {
    @SerializedName("server_response")
    @Expose
    private List<com.example.collabus.Model.ServerResponse> serverResponse = null;

    public List<com.example.collabus.Model.ServerResponse> getServerResponse() {
        return serverResponse;
    }

    public void setServerResponse(List<com.example.collabus.Model.ServerResponse> serverResponse) {
        this.serverResponse = serverResponse;
    }
}

