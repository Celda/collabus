package com.example.collabus;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.collabus.Helper.Config;
import com.example.collabus.Interface.CollabusRegister;
import com.example.collabus.Model.Example;
import com.example.collabus.Repository.SignUpRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
public class SignupActivity extends AppCompatActivity {
    private EditText editTextSurname, editTextName, editTextEmail, editTextBusiness, editTextPhone, editTextPassword, editTextConfPassword;
    private Spinner spinner;
    private Button buttonSignup;
    private TextView textViewToSignIn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);



        editTextSurname = (EditText) findViewById(R.id.editTextSurnameSignup);
        editTextName = (EditText) findViewById(R.id.editTextNameSignup);
        editTextEmail = (EditText) findViewById(R.id.editTextEmailSignup);
        editTextBusiness = (EditText) findViewById(R.id.editTextNBusinessSignup);
        editTextPhone = (EditText) findViewById(R.id.editTextPhoneSignup);
        editTextPassword = (EditText) findViewById(R.id.editTextPasswordSignup);
        editTextConfPassword = (EditText) findViewById(R.id.editTextConfPassword);

        spinner = (Spinner) findViewById(R.id.spinnerProfilTypeSignup);

        buttonSignup = (Button) findViewById(R.id.btnSignUp);
        textViewToSignIn = (TextView) findViewById(R.id.txtGoToSignin);

        String[] types = {"Choisir le type", "Particulier", "Entreprise"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, types);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        spinner.setSelection(0);

        textViewToSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignupActivity.this, SigninActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (spinner.getSelectedItem().toString() == types[2]) {
                    if (editTextBusiness.getVisibility() == View.GONE) {
                        editTextBusiness.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (editTextBusiness.getVisibility() == View.VISIBLE) {
                        editTextBusiness.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        buttonSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextSurname.getText().toString().isEmpty()) {
                    Toast.makeText(SignupActivity.this, "Veuillez renseigner votre nom.", Toast.LENGTH_SHORT).show();
                }
                if (editTextName.getText().toString().isEmpty()) {
                    Toast.makeText(SignupActivity.this, "Veuillez renseigner votre prénom.", Toast.LENGTH_SHORT).show();
                }
                if (editTextEmail.getText().toString().isEmpty()) {
                    Toast.makeText(SignupActivity.this, "Veuillez renseigner votre adresse mail.", Toast.LENGTH_SHORT).show();
                }
                if (spinner.getSelectedItem().toString() == types[2]) {
                    if (editTextBusiness.getText().toString().isEmpty()) {
                        Toast.makeText(SignupActivity.this, "Veuillez renseigner le nom de l'entreprise.", Toast.LENGTH_SHORT).show();
                    }
                }
                if (spinner.getSelectedItem().toString() == types[0]) {
                    Toast.makeText(SignupActivity.this, "Veuillez sélectionner le type de profil.", Toast.LENGTH_SHORT).show();
                }

                if (editTextPhone.getText().toString().isEmpty()) {
                    Toast.makeText(SignupActivity.this, "Veuillez renseigner votre numéro de téléphone.", Toast.LENGTH_SHORT).show();
                }

                if (editTextPassword.getText().toString().isEmpty()) {
                    Toast.makeText(SignupActivity.this, "Veuillez renseigner votre mot de passe.", Toast.LENGTH_SHORT).show();
                }

                if (editTextConfPassword.getText().toString().isEmpty()) {
                    Toast.makeText(SignupActivity.this, "Veuillez confirmer le mot de passe.", Toast.LENGTH_SHORT).show();
                }

                if (!editTextPassword.getText().toString().equals(editTextConfPassword.getText().toString())) {
                    Toast.makeText(SignupActivity.this, "Les mots de passe ne correspondent pas.", Toast.LENGTH_SHORT).show();
                }

                if (!editTextSurname.getText().toString().isEmpty() && !editTextName.getText().toString().isEmpty() &&
                        !editTextEmail.getText().toString().isEmpty() && spinner.getSelectedItem().toString() == types[1] && !editTextPhone.getText().toString().isEmpty()
                        && !editTextPassword.getText().toString().isEmpty() && !editTextConfPassword.getText().toString().isEmpty() &&
                        editTextPassword.getText().toString().equals(editTextConfPassword.getText().toString())) {

//                    SignUpRepository signUpRepository = new SignUpRepository();
//                    signUpRepository.toSgnUp(editTextSurname.getText().toString(), editTextName.getText().toString(), editTextPassword.getText().toString(),
//                            editTextEmail.getText().toString(), spinner.getSelectedItem().toString());

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Config.GET_BASE_URL_RETROFIT)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                    CollabusRegister service = retrofit.create(CollabusRegister.class);

                    Call<Example> call = service.createUser(editTextSurname.getText().toString(), editTextName.getText().toString(), editTextPassword.getText().toString(), editTextEmail.getText().toString(), spinner.getSelectedItem().toString());
                    call.enqueue(new Callback<Example>() {
                        @Override
                        public void onResponse(Call<Example> call, Response<Example> response) {
                            if (response != null) {
                                if (response.isSuccessful()) {
//                                    Log.i(TAG, response.body().toString());
//                                    Log.i(TAG, response.body().getServerResponse().toString());
                                    Config.serverResponse  = response.body().getServerResponse().get(0);
                                    Intent intent = new Intent(SignupActivity.this, DisplayDataActivity.class);
                                    intent.putExtra("sampleObject", Config.serverResponse);
                                    startActivity(intent);
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Example> call, Throwable t) {
                            // Log.e(TAG, t.getMessage());
                        }
                    });

                } else if (!editTextSurname.getText().toString().isEmpty() && !editTextName.getText().toString().isEmpty() &&
                        !editTextEmail.getText().toString().isEmpty() && spinner.getSelectedItem().toString() == types[2] &&
                        !editTextBusiness.getText().toString().isEmpty() &&!editTextPhone.getText().toString().isEmpty()
                        && !editTextPassword.getText().toString().isEmpty() && !editTextConfPassword.getText().toString().isEmpty() &&
                        editTextPassword.getText().toString().equals(editTextConfPassword.getText().toString())) {

//                    SignUpRepository signUpRepository = new SignUpRepository();
//                    signUpRepository.toSgnUp(editTextSurname.getText().toString(), editTextName.getText().toString(), editTextPassword.getText().toString(),
//                            editTextEmail.getText().toString(), spinner.getSelectedItem().toString());

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Config.GET_BASE_URL_RETROFIT)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                    CollabusRegister service = retrofit.create(CollabusRegister.class);

                    Call<Example> call = service.createUser(editTextSurname.getText().toString(), editTextName.getText().toString(), editTextPassword.getText().toString(), editTextEmail.getText().toString(), spinner.getSelectedItem().toString());
                    call.enqueue(new Callback<Example>() {
                        @Override
                        public void onResponse(Call<Example> call, Response<Example> response) {
                            if (response != null) {
                                if (response.isSuccessful()) {
//                                    Log.i(TAG, response.body().toString());
//                                    Log.i(TAG, response.body().getServerResponse().toString());
                                    Config.serverResponse  = response.body().getServerResponse().get(0);
                                    Intent intent = new Intent(SignupActivity.this, DisplayDataActivity.class);
                                    intent.putExtra("sampleObject", Config.serverResponse);
                                    startActivity(intent);
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Example> call, Throwable t) {
                            // Log.e(TAG, t.getMessage());
                        }
                    });
                }
            }
        });
    }
}