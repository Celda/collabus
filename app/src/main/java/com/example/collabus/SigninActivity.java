package com.example.collabus;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.collabus.Interface.CollabusLogin;
import com.google.android.material.button.MaterialButtonToggleGroup;
import com.example.collabus.Helper.Config;
import com.example.collabus.Interface.CollabusLogin;
import com.example.collabus.Model.Example;
import com.example.collabus.Model.ServerResponse;
import com.example.collabus.Repository.SignInRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SigninActivity extends AppCompatActivity {
    private EditText editTextEmail, editTextPassword;
    private TextView textViewGoToSignUp, textViewPasswordForget;
    private Button buttonSignin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);



        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        textViewGoToSignUp = (TextView) findViewById(R.id.txtGoToSignUp);
        textViewPasswordForget = (TextView) findViewById(R.id.txtPasswordForget);
        buttonSignin = (Button) findViewById(R.id.btnSignIn);

        textViewGoToSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SigninActivity.this, SignupActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        buttonSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextEmail.getText().toString().isEmpty()) {
                    Toast.makeText(SigninActivity.this, "Veuillez renseigner votre adresse mail.", Toast.LENGTH_SHORT).show();
                }
                if (editTextPassword.getText().toString().isEmpty()) {
                    Toast.makeText(SigninActivity.this, "Veuillez renseigner votre mot de passe.", Toast.LENGTH_SHORT).show();
                }

                if (!editTextEmail.getText().toString().isEmpty() && !editTextPassword.getText().toString().isEmpty()) {
                    /*ServerResponse[] serverResponse = {new ServerResponse()};
                    SignInRepository signInRepository = new SignInRepository();
                    serverResponse[0] = signInRepository.toSignin(editTextEmail.getText().toString(), editTextPassword.getText().toString());
                    if (serverResponse[0] != null) {
                        if (!serverResponse[0].getEmail().isEmpty()) {
                            //Config.serverResponse  = response.body().getServerResponse().get(0);
                            Intent intent = new Intent(SigninActivity.this, DisplayDataActivity.class);
                            intent.putExtra("sampleObject",serverResponse[0]);
                            startActivity(intent);
                        }
                    }*/

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Config.GET_BASE_URL_RETROFIT)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                    CollabusLogin service = retrofit.create(CollabusLogin.class);

                    Call<Example> call  = service.loginUser(editTextEmail.getText().toString(),  editTextPassword.getText().toString() );
                    call.enqueue(new Callback<Example>() {
                        @Override
                        public void onResponse(Call<Example> call, Response<Example> response) {
                            if (response != null) {
                                if (response.isSuccessful()) {
                                    // Log.i(TAG, response.body().getServerResponse().toString());
                                    Config.serverResponse  = response.body().getServerResponse().get(0);
                                    Intent intent = new Intent(SigninActivity.this, DisplayDataActivity.class);
                                    intent.putExtra("sampleObject", Config.serverResponse);
                                    startActivity(intent);
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Example> call, Throwable t) {
                            // Log.e(TAG, t.getMessage());
                        }
                    });
                }
            }
        });
    }
}