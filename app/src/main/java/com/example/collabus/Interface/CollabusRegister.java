package com.example.collabus.Interface;




import retrofit2.Call;

import com.example.collabus.Model.Example;
import com.example.collabus.Model.ServerResponse;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface CollabusRegister {
    @FormUrlEncoded
    @POST("Api/register.php")
    Call<Example> createUser(@Field("last_name") String last_name, @Field("first_name") String first_name,
                             @Field("passe") String passe, @Field("email") String email, @Field("type_compte") String type_compte);
}

