package com.example.collabus.Interface;



import com.example.collabus.Model.Example;
import com.example.collabus.Model.ServerResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface CollabusLogin {
    @FormUrlEncoded
    @POST("Api/login.php")
    Call<Example> loginUser(@Field("email") String email, @Field("passe") String password);
}



