package com.example.collabus.Repository;

import android.util.Log;

import com.example.collabus.Helper.Config;
import com.example.collabus.Interface.CollabusRegister;
import com.example.collabus.Model.Example;
import com.example.collabus.Model.ServerResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SignUpRepository {
    private static final String TAG = "SignUpRepository";

    public void toSgnUp (String last_name, String first_name, String passe, String email, String type_compte) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        CollabusRegister service = retrofit.create(CollabusRegister.class);

        Call<Example> call = service.createUser(last_name, first_name, passe, email, type_compte);
        call.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                if (response != null) {
                    if (response.isSuccessful()) {
                        Log.i(TAG, response.body().toString());
                        Log.i(TAG, response.body().getServerResponse().toString());
                        Config.serverResponse  = response.body().getServerResponse().get(1);

                    }
                }
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }
}

