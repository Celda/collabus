package com.example.collabus.Repository;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.collabus.DisplayDataActivity;
import com.example.collabus.Helper.Config;
import com.example.collabus.Interface.CollabusLogin;
import com.example.collabus.Interface.CollabusLogin;
import com.example.collabus.Model.Example;
import com.example.collabus.Model.ServerResponse;
import com.example.collabus.SigninActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SignInRepository {
    public SignInRepository() {
    }

    private static final String TAG = "SignInRepository";

    public ServerResponse toSignin (String email, String password) {
        final ServerResponse[] serverResponse = {new ServerResponse()};
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.GET_BASE_URL_RETROFIT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        CollabusLogin service = retrofit.create(CollabusLogin.class);

        Call<Example> call  = service.loginUser(email, password );
        call.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                if (response != null) {
                    if (response.isSuccessful()) {
                        Log.i(TAG, response.body().getServerResponse().toString());
                        serverResponse[0] = response.body().getServerResponse().get(0);
                        Config.serverResponse  = response.body().getServerResponse().get(0);
                    }
                }
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });

        return serverResponse[0];
    }

}

